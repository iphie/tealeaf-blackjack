# blackjack.rb

def decorate_input(input) # Decorates input and displays on same line
  print "#{input} => "
end

 def decorate_output(output) # Decorates output and drops to next line
  puts "=> #{output}"
end

def calculate_total(cards) # Nested Array
  arr = cards.map{|e| e[1]}

  total = 0
  arr.each do |value|
    if value == "A"
      total += 11
    elsif value.to_i == 0
      total += 10
    else
      total += value.to_i
    end
  end

  arr.select{|e| e == "A"}.count.times do
    if total > 21
      total -= 10
    end
  end
    total
end

# Welcome & Create Deck

decorate_output("Welcome to Blackjack!")

suits = ['H', 'D', 'S', 'C']
cards = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']

deck = suits.product(cards)
deck.shuffle!

# Deal the Cards

player_cards = []
dealer_cards = []

player_cards << deck.pop
dealer_cards << deck.pop
player_cards << deck.pop
dealer_cards << deck.pop

player_total = calculate_total(player_cards)
dealer_total = calculate_total(dealer_cards)

# Show starting cards

decorate_output("Dealer has #{dealer_cards[0]} and #{dealer_cards[1]}, for a total of #{dealer_total}")
decorate_output("You have #{player_cards[0]} and #{player_cards[1]}, for a total of #{player_total}")

# Player's Turn

if player_total == 21 && dealer_total != 21
  decorate_output("Congratulations, you got a Blackjack! You win!")
  exit
elsif player_total == 21 && dealer_total == 21
  decorate_output("Dealer and Player tied.  Push.")
elsif dealer_total == 21 && player_total != 21
  decorate_output("Dealer Blackjack.  Dealer Wins!")
  exit
else
end

while player_total < 21
  decorate_input("(H)it or (S)tand?")
  motion = gets.chomp.upcase
  if !['H', 'S'].include?(motion)
    decorate_output("Error: You must enter H for (H)it or S for (S)tand.")
    next
  end

  # Stand
  
  if motion == "S"
    decorate_output("You chose to stand with your current total of #{player_total}")
    break
  end
  
  # Hit
  
  if motion == "H" 
    new_card = deck.pop
    player_cards << new_card
    player_total = calculate_total(player_cards)
    decorate_output("New card is #{new_card} for a new total of #{player_total}.")
  end
end

if player_total > 21
  decorate_output("You bust!")
  exit
end

if dealer_total > 21
  decorate_output("Dealer Busts! You Win!")
  exit
end

until dealer_total >= 17
  new_card = deck.pop
  dealer_cards << new_card
  dealer_total = calculate_total(dealer_cards)
  decorate_output("Dealer's card is #{new_card} for a total of #{dealer_total}")
next
end
   
if dealer_total > 21
  decorate_output("Dealer Busts, you win!")
elsif player_total > dealer_total
  decorate_output("Dealer Stands with #{dealer_total}")
  decorate_output("Player Wins with #{player_total}")
elsif player_total < dealer_total
  decorate_output("Dealer stands with #{dealer_total}")
  decorate_output("Dealer Wins!")
else
  decorate_output("Dealer Stands")
  decorate_output("Push.")
end